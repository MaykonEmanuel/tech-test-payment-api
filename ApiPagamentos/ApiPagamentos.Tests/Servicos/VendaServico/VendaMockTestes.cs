﻿using ApiPagamentos.Domain.Entidades;
using System;
using System.Collections.Generic;

namespace ApiPagamentos.Testes.Servicos.VendaServico
{
    public class VendaMockTestes
    {
        public static Venda VendaPost { get; set; }
        public static Venda VendaPost2 { get; set; }
        public static Vendedor VendedorPost { get; set; }
        public static Item Item1 { get; set; }
        public static Item Item2 { get; set; }
        public static Item Item3 { get; set; }
        public static List<Item> ItensPost = new List<Item>();

        public VendaMockTestes()
        {
            Item1 = new Item
            {
                Nome = "Impressora",
                Quantidade = 15,
                Preco = 260
            };

            Item2 = new Item
            {
                Nome = "Notebook",
                Quantidade = 1,
                Preco = 3000
            };

            Item3 = new Item
            {
                Nome = "Tablet",
                Quantidade = 10,
                Preco = 1200
            };

            ItensPost.Add(Item1);
            ItensPost.Add(Item2);
            ItensPost.Add(Item3);


            VendedorPost = new Vendedor
            {
                Cpf = Faker.Identification.UKNationalInsuranceNumber(),
                Nome = Faker.Name.FullName(),
                Email = Faker.Internet.Email(),
                Telefone = Faker.Phone.Number()
            };
            VendaPost = new Venda
            {
                Id = Guid.NewGuid(),
                Vendedor = VendedorPost,
                Itens = ItensPost
            };

            VendaPost2 = new Venda
            {
                Id = Guid.NewGuid(),
                Vendedor = VendedorPost,
                Itens = ItensPost
            };
        }
    }
}
