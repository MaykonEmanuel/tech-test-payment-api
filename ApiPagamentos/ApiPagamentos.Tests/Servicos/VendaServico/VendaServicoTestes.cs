﻿using ApiPagamentos.Domain.Entidades;
using ApiPagamentos.Domain.Enumeradores;
using ApiPagamentos.Domain.Excecoes.Application;
using ApiPagamentos.Domain.IRepositorios;
using ApiPagamentos.Domain.IServicos;
using ApiPagamentos.Domain.Utilitarios;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace ApiPagamentos.Testes.Servicos.VendaServico
{
    public class VendaServicoTestes : VendaDependenciasTestes
    {
        public VendaServicoTestes()
        {
            _vendaRepositorioMock = new Mock<IVendaRepositorio>();
            _vendaServico = new Application.Servicos.VendaServico(_vendaRepositorioMock.Object);
        }

        #region MÉTODO ALTERARSTATUSVENDA
        [Fact]
        [Trait(nameof(IVendaServico.AlterarStatusVenda), "Recebe ID de uma venda inexistente e Novo Status Válido")]
        public async Task RECEBE_IDVENDA_INEXISTENTE_VALIDADO_RETORNA_EXCECAO_TRATADA()
        {
            _vendaRepositorioMock.Setup(v => v.BuscaVenda(Guid.NewGuid())).ReturnsAsync(VendaPost);

            var excecao = await Assert.ThrowsAnyAsync<ServicoCoreExcecao>(
                async () =>
                {
                    await _vendaServico.AlterarStatusVenda(Guid.NewGuid(), StatusVendaEnum.AguardandoPagamento);
                });

            Assert.Equal(ServicoCoreError.VendaNaoEncontradaPut.Chave, excecao.CoreErro.Chave);
        }

        [Fact]
        [Trait(nameof(IVendaServico.AlterarStatusVenda), "ID Válido e Novo Status Inválido devido a regra de negócio")]
        public async Task RECEBE_NOVO_STATUS_INVALIDADO_RETORNA_EXCECAO_TRATADA()
        {
            _vendaRepositorioMock.Setup(v => v.BuscaVenda(It.IsAny<Guid>())).ReturnsAsync(VendaPost);

            var excecao = await Assert.ThrowsAnyAsync<ServicoCoreExcecao>(
                async () =>
                {
                    await _vendaServico.AlterarStatusVenda(Guid.NewGuid(), StatusVendaEnum.EnviadoTransportadora);
                });

            Assert.Equal(ServicoCoreError.AtualizacaoStatusVendaPut.Chave, excecao.CoreErro.Chave);
        }

        [Fact]
        [Trait(nameof(IVendaServico.AlterarStatusVenda), "ID Válido e Novo Status Inválido devido ser o status atual da venda")]
        public async Task RECEBE_STATUS_IGUAL_AO_ATUAL_VALIDADO_RETORNA_EXCECAO_TRATADA()
        {
            _vendaRepositorioMock.Setup(v => v.BuscaVenda(It.IsAny<Guid>())).ReturnsAsync(VendaPost);

            var excecao = await Assert.ThrowsAnyAsync<ServicoCoreExcecao>(
                async () =>
                {
                    await _vendaServico.AlterarStatusVenda(Guid.NewGuid(), StatusVendaEnum.AguardandoPagamento);
                });

            Assert.Equal(ServicoCoreError.AtualizacaoParaMesmoStatusPut.Chave, excecao.CoreErro.Chave);
        }

        [Fact]
        [Trait(nameof(IVendaServico.AlterarStatusVenda), "ID Válido e Novo Status Válido")]
        public async Task RECEBE_STATUS_VALIDO_EXECUTADO_RETORNA_MENSAGEM()
        {
            _vendaRepositorioMock.Setup(v => v.BuscaVenda(It.IsAny<Guid>())).ReturnsAsync(VendaPost);
            _vendaRepositorioMock.Setup(v => v.AtualizarStatus(VendaPost, It.IsAny<StatusVendaEnum>()))
            .ReturnsAsync("Atualizado o status da Venda para : " + StatusVendaEnum.Cancelada.RetornaDescricaoEnum());

            var mensagem = await _vendaServico.AlterarStatusVenda(Guid.NewGuid(), StatusVendaEnum.Cancelada);

            Assert.NotNull(mensagem);
            Assert.True(mensagem.Length > StatusVendaEnum.Cancelada.RetornaDescricaoEnum().Length);
        }
        #endregion

        #region METODO BUSCAVENDAPELOID
        [Fact]
        [Trait(nameof(IVendaServico.BuscaVendaPeloId), "ID Inexistente da venda com isto logo não haverá retorno da mesma")]
        public async Task RECEBE_IDVENDA_INEXISTENTE_BUSCADO_RETORNA_EXCECAO_TRATADA()
        {
            _vendaRepositorioMock.Setup(v => v.BuscaVenda(Guid.NewGuid())).ReturnsAsync(VendaPost);

            var excecao = await Assert.ThrowsAnyAsync<ServicoCoreExcecao>(
                async () =>
                {
                    await _vendaServico.BuscaVendaPeloId(Guid.NewGuid());
                });

            Assert.Equal(ServicoCoreError.VendaNaoEncontradaGet.Chave, excecao.CoreErro.Chave);
        }

        [Fact]
        [Trait(nameof(IVendaServico.BuscaVendaPeloId), "ID da venda Existente, retorna uma Venda")]
        public async Task RECEBE_ID_EXISTENTE_DE_UMA_VENDA_EXECUTADO_VALIDA_SE_VENDA_NAO_E_NULA()
        {
            _vendaRepositorioMock.Setup(v => v.BuscaVenda(It.IsAny<Guid>())).ReturnsAsync(VendaPost2);
            var venda = await _vendaServico.BuscaVendaPeloId(Guid.NewGuid());

            Assert.False(venda == null);
        }
        #endregion

        #region REGISTRARVENDA
        [Fact]
        [Trait(nameof(IVendaServico.RegistrarVenda), "Recebe uma venda nula, retorna erro tratado")]
        public async Task RECEBE_VENDA_NULA_EXECUTADO_RETORNA_EXCECAO_TRATADA()
        {
            _vendaRepositorioMock.Setup(v => v.SalvarVenda(It.IsAny<Venda>())).ReturnsAsync(Guid.NewGuid());

            var excecao = await Assert.ThrowsAnyAsync<ServicoCoreExcecao>(
               async () =>
               {
                   await _vendaServico.RegistrarVenda(null);
               });

            Assert.Equal(ServicoCoreError.VendaNaoEncontradaPost.Chave, excecao.CoreErro.Chave);
        }

        [Fact]
        [Trait(nameof(IVendaServico.RegistrarVenda), "Recebe uma venda, retorna o ID")]
        public async Task RECEBE_VENDA_EXECUTADO_RETORNA_ID()
        {
            _vendaRepositorioMock.Setup(v => v.SalvarVenda(It.IsAny<Venda>())).ReturnsAsync(Guid.NewGuid());
            var id = await _vendaServico.RegistrarVenda(VendaPost);
            Assert.True(id.ToString() != null);
        }
        #endregion
    }
}
