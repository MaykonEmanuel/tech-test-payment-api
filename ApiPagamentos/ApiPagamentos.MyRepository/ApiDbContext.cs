﻿using ApiPagamentos.Domain.Entidades;
using Microsoft.EntityFrameworkCore;

namespace ApiPagamentos.MyRepository
{
    public class ApiDbContext : DbContext
    {
        public ApiDbContext(DbContextOptions<ApiDbContext> options)
            : base(options) { }

        public DbSet<Item> Itens { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
    }
}
