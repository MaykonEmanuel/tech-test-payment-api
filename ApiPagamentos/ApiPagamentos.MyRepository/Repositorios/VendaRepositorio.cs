﻿using ApiPagamentos.Domain.Entidades;
using ApiPagamentos.Domain.Enumeradores;
using ApiPagamentos.Domain.IRepositorios;
using ApiPagamentos.Domain.Utilitarios;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace ApiPagamentos.MyRepository.Repositorios
{
    public class VendaRepositorio : IVendaRepositorio
    {
        private readonly ApiDbContext _context;

        public VendaRepositorio(ApiDbContext context)
        {
            _context = context ??
                throw new ArgumentNullException(nameof(context));
        }

        public async Task<string> AtualizarStatus(Venda venda, StatusVendaEnum status)
        {
            venda.AtualizaStatus(status);
            await _context.SaveChangesAsync();
            return "Atualizado o status da Venda para : " + status.RetornaDescricaoEnum();
        }

        public async Task<Venda> BuscaVenda(Guid id)
        {
            return await _context.Vendas.
                Include(a => a.Vendedor).
                Include(a => a.Itens)
                .FirstOrDefaultAsync
                (x => x.Id == id);
        }

        public async Task<Guid> SalvarVenda(Venda venda)
        {
            await _context.Vendas.AddAsync(venda);
            await _context.SaveChangesAsync();
            return venda.Id;
        }
    }
}
