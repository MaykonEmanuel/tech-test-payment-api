﻿using ApiPagamentos.Domain.IRepositorios;
using ApiPagamentos.MyRepository.Repositorios;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ApiPagamentos.MyRepository.Dependencias
{
    public static class ApiDbContextDependencia
    {
        public static void AdicionaRepositorio(this IServiceCollection servicos)
        {
            if (servicos == null)
                throw new ArgumentException(nameof(servicos));

            servicos.AddScoped<IVendaRepositorio, VendaRepositorio>();
        }
    }
}
