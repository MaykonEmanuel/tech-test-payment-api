﻿using ApiPagamentos.Domain.Entidades;
using ApiPagamentos.Domain.Enumeradores;
using ApiPagamentos.Domain.Excecoes.Application;
using ApiPagamentos.Domain.IRepositorios;
using ApiPagamentos.Domain.IServicos;
using System;
using System.Threading.Tasks;

namespace ApiPagamentos.Application.Servicos
{
    public class VendaServico : IVendaServico
    {
        private readonly IVendaRepositorio _vendaRepositorio;

        public VendaServico(IVendaRepositorio vendaRepositorio)
        {
            _vendaRepositorio = vendaRepositorio ??
                throw new ArgumentException(nameof(vendaRepositorio));
        }

        public async Task<string> AlterarStatusVenda(Guid id, StatusVendaEnum statusNovo)
        {

            if (string.IsNullOrEmpty(id.ToString()))
                throw new ServicoCoreExcecao(ServicoCoreError.IdInvalidoPut);

            var venda = await _vendaRepositorio.BuscaVenda(id);

            if (venda == null)
                throw new ServicoCoreExcecao(ServicoCoreError.VendaNaoEncontradaPut);

            if (venda.ValidaAtualizacaoStatusVenda(venda.StatusVenda, statusNovo))
            {
                var retorno = await _vendaRepositorio.AtualizarStatus(venda, statusNovo);
                return retorno;
            }
            else
            {
                if (venda.StatusVenda == statusNovo)
                    throw new ServicoCoreExcecao(ServicoCoreError.AtualizacaoParaMesmoStatusPut);
                throw new ServicoCoreExcecao(ServicoCoreError.AtualizacaoStatusVendaPut);
            }
        }


        public async Task<Venda> BuscaVendaPeloId(Guid id)
        {
            if (string.IsNullOrEmpty(id.ToString()))
                throw new ServicoCoreExcecao(ServicoCoreError.IdInvalidoGet);

            var venda = await _vendaRepositorio.BuscaVenda(id);

            if (venda == null)
                throw new ServicoCoreExcecao(ServicoCoreError.VendaNaoEncontradaGet);
            return (venda);
        }

        public async Task<Guid> RegistrarVenda(Venda venda)
        {
            if (venda == null)
                throw new ServicoCoreExcecao(ServicoCoreError.VendaNaoEncontradaPost);

            if (venda.Itens.Count <= 0 || venda.Itens == null)
                throw new ServicoCoreExcecao(ServicoCoreError.ItemVazioOuNuloPost);
            else
            {
                foreach (var item in venda.Itens)
                {
                    if (string.IsNullOrEmpty(item.Nome) || item.Nome.Length < 3)
                        throw new ServicoCoreExcecao(ServicoCoreError.ProdutoNomeInvalidoPost);
                    if (item.Quantidade <= 0)
                        throw new ServicoCoreExcecao(ServicoCoreError.QuantidadeItemInvalidoPost);
                    if (item.Preco <= 0)
                        throw new ServicoCoreExcecao(ServicoCoreError.PrecoItemInvalidoPost);
                }
            }

            var id = await _vendaRepositorio.SalvarVenda(venda);
            return id;
        }
    }
}
