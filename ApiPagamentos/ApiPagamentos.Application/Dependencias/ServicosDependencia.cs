﻿using ApiPagamentos.Application.Servicos;
using ApiPagamentos.Domain.IServicos;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ApiPagamentos.Application.Dependencias
{
    public static class ServicosDependencia
    {
        public static void AdicionaServicos(this IServiceCollection servicos)
        {
            if (servicos == null)
                throw new ArgumentException(nameof(servicos));

            servicos.AddScoped<IVendaServico, VendaServico>();
        }
    }
}
