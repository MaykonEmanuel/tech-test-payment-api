﻿using ApiPagamentos.Domain.Entidades;
using AutoMapper;
using WebApi_Pagamentos.Dtos.Get;
using WebApi_Pagamentos.Dtos.Post;

namespace WebApi_Pagamentos
{
    public class WebApiMapeadorObjetos : Profile
    {
        public WebApiMapeadorObjetos()
        {

            #region Post
            CreateMap<ItemPostDto, Item >();
            CreateMap<VendaPostDto, Venda>();
            CreateMap<VendedorPostDto, Vendedor>();
            #endregion

            #region Get
            CreateMap<ItemGetDto, Item>().ReverseMap();
            CreateMap<VendaGetDto, Venda>().ReverseMap();
            CreateMap<VendedorGetDto, Vendedor>().ReverseMap();
            #endregion

        }
    }
}
