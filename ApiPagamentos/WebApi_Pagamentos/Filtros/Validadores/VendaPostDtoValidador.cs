﻿using ApiPagamentos.Domain.Utilitarios;
using FluentValidation;
using WebApi_Pagamentos.Dtos.Post;

namespace WebApi_Pagamentos.Filtros.Validadores
{
    public class VendaPostDtoValidador : AbstractValidator<VendaPostDto>
    {
        public VendaPostDtoValidador()
        {

            #region Validacoes Vendedor
            RuleFor(v => v.Vendedor.Nome)
                .NotEmpty()
                .NotNull()
                    .WithMessage("O nome do Vendedor não pode ser nulo ou vazio")
                .MaximumLength(40)
                    .WithMessage("O nome do Vendedor não pode ter mais de 40 caracteres")
                .MinimumLength(3)
                    .WithMessage("O nome do Vendedor deve conter mais de 2 caracteres");

            RuleFor(v => v.Vendedor.Cpf)
                .Must(v => v.CPFValido())
                .MaximumLength(14)
                    .WithMessage("CPF inválido");

            RuleFor(v => v.Vendedor.Email)
                .NotEmpty()
                .NotNull()
                    .WithMessage("O Email do Vendedor não pode ser nulo ou vazio")
                .Must(v => v.EmailValido())
                    .WithMessage("Email inválido");

            RuleFor(v => v.Vendedor.Telefone)
                .NotEmpty()
                .NotNull()
                    .WithMessage("O Telefone do Vendedor não pode ser nulo ou vazio")
                .Must(v => v.TelefoneValido())
                    .WithMessage("Telefone inválido");
            #endregion
        }
    }
}
