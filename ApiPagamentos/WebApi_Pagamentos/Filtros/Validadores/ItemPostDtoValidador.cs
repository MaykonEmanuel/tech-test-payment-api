﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_Pagamentos.Dtos.Post;

namespace WebApi_Pagamentos.Filtros.Validadores
{
    public class ItemPostDtoValidador : AbstractValidator<ItemPostDto>
    {
        public ItemPostDtoValidador()
        {

            RuleFor(i => i.Nome)
                 .NotEmpty()
                .NotNull()
                    .WithMessage("O nome do Item não pode ser nulo ou vazio")
                .MaximumLength(40)
                    .WithMessage("O nome do Item não pode ter mais de 40 caracteres")
                .MinimumLength(3)
                    .WithMessage("O nome do Item deve conter mais de 2 caracteres");

        }
    }
}
