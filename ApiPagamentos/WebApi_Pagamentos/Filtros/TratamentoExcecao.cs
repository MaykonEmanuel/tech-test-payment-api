﻿using ApiPagamentos.Domain.Excecoes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace WebApi_Pagamentos.Filtros
{
    public class TratamentoExcecao : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            if (context.Exception is CoreExcecao exception)
            {
                Console.WriteLine($"ERRONEGOCIO : { exception.CoreErro.Chave} - { exception.CoreErro.Mensagem}");

                context.Result = new JsonResult(exception.CoreErro)
                {
                    StatusCode = 400
                };
            }
            else
            {
                Console.WriteLine($"ERROAPI : {context.Exception.Message}");
            }
        }
    }
}
