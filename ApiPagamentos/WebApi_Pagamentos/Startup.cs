using ApiPagamentos.Application.Dependencias;
using ApiPagamentos.MyRepository;
using ApiPagamentos.MyRepository.Dependencias;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;
using WebApi_Pagamentos.Filtros;
using WebApi_Pagamentos.Filtros.Validadores;

namespace WebApi_Pagamentos
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers()
            .AddFluentValidation(c => c.RegisterValidatorsFromAssemblyContaining<VendaPostDtoValidador>());
            services.AddAutoMapper(typeof(WebApiMapeadorObjetos));
            services.AddDbContext<ApiDbContext>(opt => opt.UseInMemoryDatabase("ApiPagamentos"));
            services.AddMvc(opcoes =>
            {
                opcoes.Filters.Add(new TratamentoExcecao());
            });
            DependenciasProjeto(services);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "WebApi_Pagamentos", Version = "v1" });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        public void DependenciasProjeto(IServiceCollection services)
        {
            services.AdicionaRepositorio();
            services.AdicionaServicos();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "WebApi_Pagamentos v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
