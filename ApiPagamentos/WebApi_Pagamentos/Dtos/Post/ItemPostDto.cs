﻿namespace WebApi_Pagamentos.Dtos.Post
{
    public class ItemPostDto
    {
        public string Nome { get; set; }
        public int Quantidade { get; set; }
        public decimal Preco { get; set; }
    }
}
