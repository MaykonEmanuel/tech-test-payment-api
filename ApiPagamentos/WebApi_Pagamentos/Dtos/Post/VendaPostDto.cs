﻿using System.Collections.Generic;

namespace WebApi_Pagamentos.Dtos.Post
{
    public class VendaPostDto
    {
        public VendedorPostDto Vendedor { get; set; }
        public List<ItemPostDto> Itens { get; set; }
    }
}
