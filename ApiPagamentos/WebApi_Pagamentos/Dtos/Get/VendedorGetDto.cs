﻿namespace WebApi_Pagamentos.Dtos.Get
{
    public class VendedorGetDto
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
    }
}
