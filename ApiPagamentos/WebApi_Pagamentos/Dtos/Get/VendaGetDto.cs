﻿using ApiPagamentos.Domain.Enumeradores;
using System.Collections.Generic;

namespace WebApi_Pagamentos.Dtos.Get
{
    public class VendaGetDto
    {
        public VendedorGetDto Vendedor { get; set; }
        public List<ItemGetDto> Itens { get; set; }
        public StatusVendaEnum StatusVenda { get; set; }
    }
}
