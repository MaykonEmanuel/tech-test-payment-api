﻿namespace WebApi_Pagamentos.Dtos.Get
{
    public class ItemGetDto
    {
        public string Nome { get; set; }
        public int Quantidade { get; set; }
        public decimal Preco { get; set; }
    }
}
