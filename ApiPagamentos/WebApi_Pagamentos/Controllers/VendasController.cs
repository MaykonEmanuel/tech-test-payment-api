﻿using ApiPagamentos.Domain.Entidades;
using ApiPagamentos.Domain.Enumeradores;
using ApiPagamentos.Domain.IServicos;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using WebApi_Pagamentos.Dtos.Get;
using WebApi_Pagamentos.Dtos.Post;

namespace WebApi_Pagamentos.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendasController : ControllerBase
    {
        private readonly IVendaServico _vendaService;
        private readonly IMapper _mapper;

        public VendasController(IVendaServico vendaService, IMapper mapper)
        {
            _vendaService = vendaService ??
                 throw new ArgumentException(nameof(vendaService));

            _mapper = mapper ??
                throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Busca uma Venda pelo seu ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("BuscarVendaPeloId")]
        [ProducesResponseType(typeof(VendaGetDto), 200)]
        public async Task<IActionResult> GetVenda(Guid id)
        {
            try
            {
                var venda = await _vendaService.BuscaVendaPeloId(id);
                var vendaGet = _mapper.Map<VendaGetDto>(venda);
                return StatusCode(200, vendaGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        /// <summary>
        /// Realiza o registro de uma nova Venda
        /// </summary>
        /// <param name="venda"></param>
        /// <returns></returns>
        [HttpPost("CadastrarVenda")]
        public async Task<IActionResult> PostVenda(VendaPostDto venda)
        {
            try
            {
                var id = await _vendaService.RegistrarVenda(_mapper.Map<Venda>(venda));
                return StatusCode(201, id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        /// <summary>
        /// Realiza a atualização do Status de uma venda existente :
        /// 1 Cancelada - 2 Aguardando Pagamento - 3 Pagamento Aprovado - 4 Enviado Transportadora - 5 Entregue
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpPut("AtualizarStatus")]
        public async Task<IActionResult> PutStatus(Guid id, StatusVendaEnum status)
        {
            try
            {
                var retornoAtualizacao = await _vendaService.AlterarStatusVenda(id, status);
                return StatusCode(200, retornoAtualizacao);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }
    }
}
