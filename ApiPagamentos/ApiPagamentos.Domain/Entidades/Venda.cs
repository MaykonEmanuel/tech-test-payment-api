﻿using ApiPagamentos.Domain.Enumeradores;
using System;
using System.Collections.Generic;

namespace ApiPagamentos.Domain.Entidades
{
    public class Venda : Base
    {
        public DateTime DataCadastro { get; private set; } = DateTime.Now;
        public StatusVendaEnum StatusVenda { get; private set; } = StatusVendaEnum.AguardandoPagamento;
        public virtual Vendedor Vendedor { get; set; }
        public List<Item> Itens { get; set; }

        public bool ValidaAtualizacaoStatusVenda(StatusVendaEnum statusAtual, StatusVendaEnum statusNovo)
        {
            var statusPermitidoAguardandoPagamento = new List<StatusVendaEnum>
            {
            StatusVendaEnum.PagamentoAprovado,
            StatusVendaEnum.Cancelada
            };

            var statusPermitidoPagamentoAprovado = new List<StatusVendaEnum>
            {
            StatusVendaEnum.EnviadoTransportadora,
            StatusVendaEnum.Cancelada
            };

            var statusPermitidoEnviadoTransportadora = new List<StatusVendaEnum>
            {
            StatusVendaEnum.Entregue
            };

            if (statusAtual == StatusVendaEnum.AguardandoPagamento
                || statusAtual == StatusVendaEnum.PagamentoAprovado
                || statusAtual == StatusVendaEnum.EnviadoTransportadora)
            {
                if (statusAtual == StatusVendaEnum.AguardandoPagamento)
                {
                    foreach (var x in statusPermitidoAguardandoPagamento)
                    {
                        if (x == statusNovo)
                            return true;
                    }
                }
                else if (statusAtual == StatusVendaEnum.PagamentoAprovado)
                {
                    foreach (var x in statusPermitidoPagamentoAprovado)
                    {
                        if (x == statusNovo)
                            return true;
                    }
                }
                else
                {
                    foreach (var x in statusPermitidoEnviadoTransportadora)
                    {
                        if (x == statusNovo)
                            return true;
                    }
                }
                return false;
            }
            else
            {
                return true;
            }
        }

        public void AtualizaStatus(StatusVendaEnum novoStatus)
        {
            StatusVenda = novoStatus;
        }
    }
}
