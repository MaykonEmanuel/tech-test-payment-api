﻿namespace ApiPagamentos.Domain.Entidades
{
    public class Item : Base
    {
        public string Nome { get; set; }
        public int Quantidade { get; set; }
        public decimal Preco { get; set; }
    }
}
