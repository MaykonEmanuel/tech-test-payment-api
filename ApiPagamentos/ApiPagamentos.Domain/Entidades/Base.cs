﻿using System;

namespace ApiPagamentos.Domain.Entidades
{
    public class Base
    {
        public Guid Id { get; set; } = Guid.NewGuid();
    }
}
