﻿using ApiPagamentos.Domain.Entidades;
using ApiPagamentos.Domain.Enumeradores;
using System;
using System.Threading.Tasks;

namespace ApiPagamentos.Domain.IServicos
{
    public interface IVendaServico
    {
        Task<Guid> RegistrarVenda(Venda venda);
        Task<Venda> BuscaVendaPeloId(Guid id);
        Task<string> AlterarStatusVenda(Guid id, StatusVendaEnum status);
    }
}
