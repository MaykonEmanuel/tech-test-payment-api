﻿using System;

namespace ApiPagamentos.Domain.Excecoes
{
    public class CoreExcecao : Exception
    {
        public CoreExcecaoModelo CoreErro { get; set; }

        public CoreExcecao(CoreExcecaoModelo coreerro)
        {
            CoreErro = coreerro;
        }
        public CoreExcecao() { }
        public CoreExcecao(string mensagem) : base(mensagem) { }
        public CoreExcecao(string mensagem, Exception exc) : base(mensagem, exc) { }
        protected CoreExcecao(
        System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext contexto) : base(info, contexto) { }
    }
}
