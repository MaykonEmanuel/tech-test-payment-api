﻿namespace ApiPagamentos.Domain.Excecoes.Application
{
    public class ServicoCoreExcecao : CoreExcecao
    {
        public ServicoCoreExcecao(ServicoCoreError error) : base(error) { }
    }
    public class ServicoCoreError : CoreExcecaoModelo
    {
        public static ServicoCoreError IdInvalidoPut
            = new ServicoCoreError("IdInvalido", "Id inexistente e\\ou inválido");
        public static ServicoCoreError IdInvalidoGet
           = new ServicoCoreError("IdInvalidoGet", "Id inexistente e\\ou inválido");
        public static ServicoCoreError VendaNaoEncontradaGet
           = new ServicoCoreError("VendaNaoEncontradaGet", "Não foi localizada a Venda");
        public static ServicoCoreError VendaNaoEncontradaPost
            = new ServicoCoreError("VendaNaoEncontradaPost", "Não foi localizada a Venda");
        public static ServicoCoreError VendaNaoEncontradaPut
            = new ServicoCoreError("VendaNaoEncontradaPut", "Não foi localizada a Venda");
        public static ServicoCoreError ItemVazioOuNuloPost
            = new ServicoCoreError("ItemVazioOuNuloPost", "Não é possivel cadastrar uma Venda sem Itens");
        public static ServicoCoreError AtualizacaoParaMesmoStatusPut
           = new ServicoCoreError("AtualizacaoParaMesmoStatusPut", "Não é possivel atualizar o Status da venda para o Status atual");
        public static ServicoCoreError AtualizacaoStatusVendaPut
            = new ServicoCoreError("AtualizacaoStatusVendaPut", "Não é possivel realizar esta atualização de Status");
        public static ServicoCoreError ProdutoNomeInvalidoPost
        = new ServicoCoreError("ProdutoNomeInvalidoPost", "Nome de um dos produtos de sua Venda está inválido");
        public static ServicoCoreError QuantidadeItemInvalidoPost
       = new ServicoCoreError("QuantidadeItemInvalidoPost", "Quantidade de um dos produtos de sua Venda está inválido");
        public static ServicoCoreError PrecoItemInvalidoPost
      = new ServicoCoreError("PrecoItemInvalidoPost", "Preço de um dos produtos de sua Venda está inválido");

        private ServicoCoreError(string chave, string mensagem)
            : base(chave, mensagem) { }
    }
}
