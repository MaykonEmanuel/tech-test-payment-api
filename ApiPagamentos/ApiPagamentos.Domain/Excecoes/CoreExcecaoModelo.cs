﻿namespace ApiPagamentos.Domain.Excecoes
{
    public class CoreExcecaoModelo
    {
        public string Chave { get; set; }
        public string Mensagem { get; set; }

        public CoreExcecaoModelo(string chave, string mensagem)
        {
            Chave = chave;
            Mensagem = mensagem;
        }
    }
}
