﻿using ApiPagamentos.Domain.Entidades;
using ApiPagamentos.Domain.Enumeradores;
using System;
using System.Threading.Tasks;

namespace ApiPagamentos.Domain.IRepositorios
{
    public interface IVendaRepositorio
    {
        Task<Guid> SalvarVenda(Venda venda);
        Task<Venda> BuscaVenda(Guid id);
        Task<string> AtualizarStatus(Venda venda, StatusVendaEnum status);
    }
}
