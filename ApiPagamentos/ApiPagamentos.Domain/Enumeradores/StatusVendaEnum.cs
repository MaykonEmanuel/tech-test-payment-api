﻿using System.ComponentModel.DataAnnotations;

namespace ApiPagamentos.Domain.Enumeradores
{
    public enum StatusVendaEnum
    {
        [Display(Name = "Cancelada")]
        Cancelada = 1,
        [Display(Name = "Aguardando Pagamento")]
        AguardandoPagamento = 2,
        [Display(Name = "Pagamento Aprovado")]
        PagamentoAprovado = 3,
        [Display(Name = "Enviado para Transportadora")]
        EnviadoTransportadora = 4,
        [Display(Name = "Entregue")]
        Entregue = 5,
    }
}
