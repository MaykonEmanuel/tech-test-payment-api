﻿
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace ApiPagamentos.Domain.Utilitarios
{
    public static class EnumExtensao
    {
        public static string RetornaDescricaoEnum(this System.Enum meuEnum)
        {
            return meuEnum.GetType()
                .GetMember(meuEnum.ToString())
                .First()
                .GetCustomAttribute<DisplayAttribute>()
                .GetName();
        }
    }
}
