﻿using System.Text.RegularExpressions;

namespace ApiPagamentos.Domain.Utilitarios
{
    public static class StringExtensao
    {
        public static bool CPFValido(this string cpf)
        {
            var somenteNumerosCpf = cpf.Replace(".", "").Replace("-", "").Trim();
            var condicao = @"[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}";
            return Regex.Match(somenteNumerosCpf, condicao).Success;
        }

        public static bool EmailValido(this string email)
        {
            var condicao = @"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$";
            return Regex.Match(email, condicao).Success;
        }

        public static bool TelefoneValido(this string telefone)
        {
            var condicao = @"^\(?[1-9]{2}\)? ?(?:[2-8]|9[1-9])[0-9]{3}\-?[0-9]{4}$";
            return Regex.Match(telefone.Trim(), condicao).Success;
        }
    }
}
